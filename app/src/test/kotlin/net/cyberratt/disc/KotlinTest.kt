package net.cyberratt.disc

import com.squareup.moshi.Moshi
import net.cyberratt.disc.network.typeadapter.JodaDateTimeMoshiAdapter
import net.cyberratt.disc.network.typeadapter.UuidMoshiAdapter

open class KotlinTest {
  val moshi: Moshi = Moshi.Builder()
      .add(JodaDateTimeMoshiAdapter())
      .add(UuidMoshiAdapter())
      .build()
}

