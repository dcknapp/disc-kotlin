package net.cyberratt.disc.util

import android.util.SparseIntArray
import net.cyberratt.disc.KotlinTest
import net.cyberratt.disc.db.model.Score
import net.cyberratt.disc.db.model.User
import org.hamcrest.Matchers.*
import org.junit.Assert.assertThat
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config
import java.util.ArrayList
import java.util.UUID
import kotlin.NoSuchElementException
import kotlin.test.assertEquals
import kotlin.test.fail

@RunWith(RobolectricTestRunner::class)
@Config(manifest= Config.NONE)
class ScoreUtilTest: KotlinTest() {

  @Test
  fun getFirstBackHalfHoleNumber_success() {
    assertEquals(10, ScoreUtil.getFirstBackHalfHoleNumber(17).toLong())
    assertEquals(10, ScoreUtil.getFirstBackHalfHoleNumber(18).toLong())
    assertEquals(11, ScoreUtil.getFirstBackHalfHoleNumber(19).toLong())
  }

  @Test
  fun getLowestScore_success() {
    val highScore = Score()
    highScore.scoreString = buildScoreString(18, 3)
    val mediumScore = Score()
    mediumScore.scoreString = buildScoreString(18, 2)
    val lowScore = Score()
    lowScore.scoreString = buildScoreString(18, 1)
    val scores = ArrayList<Score>()
    scores.add(highScore)
    scores.add(mediumScore)
    scores.add(lowScore)

    val result = ScoreUtil.getLowestScore(scores)
    assertThat(lowScore.totalScore, equalTo(result))
  }

  @Test
  fun getLowestScore_withTie() {
    val highScore = Score()
    highScore.scoreString = buildScoreString(18, 3)
    val firstLowScore = Score()
    firstLowScore.scoreString = buildScoreString(18, 2)
    val secondLowScore = Score()
    secondLowScore.scoreString = buildScoreString(18, 2)
    val scores = ArrayList<Score>()
    scores.add(highScore)
    scores.add(firstLowScore)
    scores.add(secondLowScore)

    val result = ScoreUtil.getLowestScore(scores)
    assertThat(secondLowScore.totalScore, equalTo(result))
    assertThat(firstLowScore.totalScore, equalTo(result))
  }

  @Test
  fun getSecondLowestScore_success() {
    val highScore = Score()
    highScore.scoreString = buildScoreString(18, 3)
    val mediumScore = Score()
    mediumScore.scoreString = buildScoreString(18, 2)
    val lowScore = Score()
    lowScore.scoreString = buildScoreString(18, 1)
    val scores = ArrayList<Score>()
    scores.add(mediumScore)
    scores.add(highScore)
    scores.add(lowScore)

    val result = ScoreUtil.getSecondLowestScore(scores)

    assertThat(mediumScore.totalScore, equalTo(result))
  }

  @Test
  fun getSecondLowestScore_onlyOneScore_success() {
    val score = Score()
    score.scoreString = buildScoreString(18, 3)
    val scores = ArrayList<Score>()
    scores.add(score)

    val result = ScoreUtil.getSecondLowestScore(scores)
    assertThat(score.totalScore, equalTo(result))
    assertThat(scores.size, equalTo(1))
  }

  @Test
  fun getSecondLowestScore_twoScores_success() {
    val secondLowestScore = Score()
    secondLowestScore.scoreString = buildScoreString(18, 3)
    val lowScore = Score()
    lowScore.scoreString = buildScoreString(18, 1)
    val scores = ArrayList<Score>()
    scores.add(secondLowestScore)
    scores.add(lowScore)

    val result = ScoreUtil.getSecondLowestScore(scores)
    assertThat(secondLowestScore.totalScore, equalTo(result))
  }

  @Test
  fun getSecondLowestScore_differentOrder_success() {
    val secondLowestScore = Score()
    secondLowestScore.scoreString = buildScoreString(18, 3)
    val lowScore = Score()
    lowScore.scoreString = buildScoreString(18, 1)
    val scores = ArrayList<Score>()
    scores.add(lowScore)
    scores.add(secondLowestScore)

    val result = ScoreUtil.getSecondLowestScore(scores)
    assertThat(secondLowestScore.totalScore, equalTo(result))
  }

  @Test
  fun isScoreTied_notTied() {
    val highScore = Score()
    highScore.scoreString = buildScoreString(18, 3)
    val mediumScore = Score()
    mediumScore.scoreString = buildScoreString(18, 2)
    val lowScore = Score()
    lowScore.scoreString = buildScoreString(18, 1)
    val scores = ArrayList<Score>()
    scores.add(mediumScore)
    scores.add(highScore)
    scores.add(lowScore)

    val result = ScoreUtil.isScoreTied(scores)
    assertThat(false, equalTo(result))
    assertThat(highScore.totalScore, not(equalTo(lowScore.totalScore)))
  }

  @Test
  fun isScoreTied_scoreIsTied() {
    val firstScore = Score()
    firstScore.scoreString = buildScoreString(18, 3)
    val secondScore = Score()
    secondScore.scoreString = buildScoreString(18, 3)
    val scores = ArrayList<Score>()
    scores.add(firstScore)
    scores.add(secondScore)

    val result = ScoreUtil.isScoreTied(scores)
    assertThat(true, equalTo(result))
    assertThat(firstScore.totalScore, equalTo(secondScore.totalScore))
  }

  @Test
  fun isScoreTied_onePlayer_notTied() {
    val score = Score()
    score.scoreString = buildScoreString(18, 3)
    val scores = listOf(score)

    val result = ScoreUtil.isScoreTied(scores)
    assertThat(false, equalTo(result))
    assertThat(scores.size, equalTo(1))

  }

  @Test
  fun scoreMapToDelimitedString_success() {
    val scoreMap = SparseIntArray()
    for (i in 0..17) {
      scoreMap.put(i, 3)
    }

    val result = ScoreUtil.toDelimitedString(scoreMap)
    assertThat(result, not(isEmptyOrNullString()))
  }

  @Test
  fun scoreStringToScoreMap_success() {
    val scoreMap = ScoreUtil.toHoleNumberedMap(buildScoreString(18, 3))
    assertThat(scoreMap, `is`(notNullValue()))
    assertThat(scoreMap.size(), equalTo(18))
    for (i in 1..18) {
      assertThat(scoreMap.get(i, 0), equalTo(3))
    }
  }

  @Test
  fun scoreStringToListOfScores_success() {
    val scoreString = buildScoreString(18, 3)
    val result = ScoreUtil.toScoreList(scoreString)
    assertThat(result, `is`(notNullValue()))
    assertThat(result, hasSize<Int>(18))
    assertThat(result, everyItem(equalTo(3)))
  }

  @Test
  fun getBackHalfScore_success() {
    val score = buildScoreString(9, 3) + "|" + buildScoreString(9, 2)

    val result = ScoreUtil.getBackHalfScore(score)
    assertThat(result, equalTo(9 * 2))
  }

  @Test
  fun getFrontHalfScore_success() {
    val score = buildScoreString(9, 3) + "|" + buildScoreString(9, 2)

    val result = ScoreUtil.getFrontHalfScore(score)
    assertThat(result, equalTo(9 * 3))
  }

  @Test
  fun calculateTotalScore_nineHoleCourse_halfScoreIsTotalScore() {
    val score = buildScoreString(9, 3)
    val expectedResult = 9 * 3
    val frontResult = ScoreUtil.getFrontHalfScore(score)
    val backResult = ScoreUtil.getBackHalfScore(score)

    assertThat(expectedResult, both(equalTo(frontResult)).and(equalTo(backResult)))
  }

  @Test
  fun calculateTotalScore_success() {
    val fiftyFour = buildScoreString(18, 3)
    val thirtySix = buildScoreString(18, 2)

    val fiftyFourResult = ScoreUtil.getTotalScore(fiftyFour)
    val thirtySixResult = ScoreUtil.getTotalScore(thirtySix)

    assertThat(fiftyFourResult, equalTo(18 * 3))
    assertThat(thirtySixResult, equalTo(18 * 2))
  }

  @Test
  fun buildDefaultScoreString_success() {
    val scoreString = ScoreUtil.buildDefaultScoreString()
    val scoreList = ScoreUtil.toScoreList(scoreString)
    val scoreValue = ScoreUtil.getTotalScore(scoreString)
    assertThat(scoreList, hasSize<Int>(18))
    assertThat(scoreValue, equalTo(0))
  }

  @Test
  fun buildDefaultScoreforUser_success() {
    val user = User()
    user.firstName = "Test"
    user.lastName = "User"

    val result = ScoreUtil.buildDefaultScoreForUser(user)
    assertThat<User>(result.user, equalTo(user))
    assertThat(result.totalScore, equalTo(0))
  }

  @Test
  fun findScoreForUser_success() {
    val playerOne = User()
    val playerTwo = User()
    val playerOneScore = Score(user = playerOne)
    val playerTwoScore = Score(user = playerTwo)

    val scores = ArrayList<Score>()
    scores.add(playerOneScore)
    scores.add(playerTwoScore)

    val result = ScoreUtil.getScoreForUser(scores, playerOne)
    assertThat<User>(result.user, equalTo(playerOne))
  }

  @Test(expected = NoSuchElementException::class)
  fun getScoreForUser_userNotFound_throwsException() {
    val user = User(id = UUID.fromString("11111111-1111-1111-1111-111111111111"))
    val score = Score()
    score.user = user
    val scores = ArrayList<Score>()
    scores.add(score)
    val notFoundUser = User(id = UUID.fromString("00000000-0000-0000-0000-000000000000"))

    ScoreUtil.getScoreForUser(scores, notFoundUser)
    fail("Should have throw exception")
  }

  @Test
  fun calculateScoreDifferences_success() {
    val numHoles = 18
    val firstPlaceUser = User(id = UUID.fromString("00000000-0000-0000-0000-000000000000"))
    val firstPlaceScore = Score(scoreString = buildScoreString(18, 1),
      user = firstPlaceUser)
    val secondPlaceUser = User(id = UUID.fromString("11111111-1111-1111-1111-111111111111"))
    val secondPlaceScore = Score(scoreString = buildScoreString(18, 2),
      user = secondPlaceUser)
    val scores = listOf(firstPlaceScore, secondPlaceScore)

    val firstPlaceDifference = ScoreUtil.calculateScoreDifference(scores, firstPlaceUser)
    assertThat(firstPlaceDifference, equalTo(18 - numHoles * 2)) // 18 under
    val secondPlaceDifference = ScoreUtil.calculateScoreDifference(scores, secondPlaceUser)
    assertThat(secondPlaceDifference, equalTo(numHoles * 2 - 18)) // 18 over
  }

  private fun buildScoreString(numHoles: Int, holeScore: Int): String {
    var score = ""
    for (i in 0 until numHoles) {
      score += holeScore.toString()
      if (i < numHoles - 1)
        score += "|"
    }

    return score
  }
}
