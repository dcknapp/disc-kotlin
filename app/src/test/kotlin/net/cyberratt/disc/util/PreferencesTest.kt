package net.cyberratt.disc.util

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.eq
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import net.cyberratt.disc.KotlinTest
import net.cyberratt.disc.db.model.User
import org.hamcrest.CoreMatchers.*
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers.anyString
import org.mockito.Mockito.doNothing
import java.util.*

class PreferencesTest: KotlinTest() {

  private var mockContext: Context = mock()
  private var mockPrefs: SharedPreferences = mock()
  private var mockPrefsEditor: SharedPreferences.Editor = mock()

  private var preferences: Preferences = Preferences(mockPrefs, moshi, mockContext)

  @SuppressLint("CommitPrefEdits")
  @Before
  fun setup() {
    whenever(mockPrefs.edit()).thenReturn(mockPrefsEditor)
    whenever(mockPrefsEditor.putString(anyString(), anyString())).thenReturn(mockPrefsEditor)
    doNothing().whenever(mockPrefsEditor).apply()
    whenever(mockPrefsEditor.commit()).thenReturn(true)
  }

  @Test
  fun getLoggedInUser_success() {
    val user = User(
      firstName = "Test",
      lastName = "User",
      email = "TestUser@testemail.com"
    )

    whenever(
        mockPrefs.getString(any(), eq(null)))
        .thenReturn(moshi.toJson(user))

    val result = preferences.loggedInUser

    println(result == null)

    assertThat(result, `is`(notNullValue()))
    assertThat<String>(result?.firstName, equalTo<String>(user.firstName))
    assertThat<String>(result?.lastName, equalTo<String>(user.lastName))
    assertThat<String>(result?.email, equalTo<String>(user.email))
    assertThat<UUID>(result?.id, equalTo<UUID>(user.id))
  }

  @Test
  fun setUserAsLoggedIn_success() {
    val user = User(
      firstName = "Test",
      lastName = "User",
      email = "TestUser@testemail.com"
    )

    preferences.loggedInUser = user

    whenever(mockPrefs.getString(any(), eq(null)))
        .thenReturn(moshi.toJson(user))

    val result = preferences.loggedInUser

    assertThat(result, `is`(notNullValue()))
    assertThat<String>(result?.firstName, equalTo<String>(user.firstName))
    assertThat<String>(result?.lastName, equalTo<String>(user.lastName))
    assertThat<String>(result?.email, equalTo<String>(user.email))
    assertThat<UUID>(result?.id, equalTo<UUID>(user.id))
  }

  @Test
  fun getLoggedInOrDefaultUser_noLoggedInUser() {
    val zeroUuid = "00000000-0000-0000-0000-000000000000"
    whenever(mockPrefs.getString(any(), any())).thenReturn(null)
    whenever(mockContext.getString(any()))
        .thenReturn("Player 1")
        .thenReturn(zeroUuid)

    val result = preferences.loggedInOrDefaultUser

    assertThat(result, `is`(notNullValue()))
    assertThat<String>(result.firstName, equalTo("Player"))
    assertThat<String>(result.lastName, equalTo("1"))
    assertThat<UUID>(result.id, equalTo(UUID.fromString(zeroUuid)))
  }

}
