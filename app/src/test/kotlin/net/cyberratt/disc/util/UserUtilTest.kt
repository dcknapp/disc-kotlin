package net.cyberratt.disc.util

import android.content.Context
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import net.cyberratt.disc.KotlinTest
import org.hamcrest.CoreMatchers.*
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Assert.fail
import org.junit.Test
import org.mockito.ArgumentMatchers
import java.util.*

class UserUtilTest: KotlinTest() {

  private val mockContext: Context = mock()

  @Test
  fun createUserWithName_onlyFirstName() {
    val firstName = "Betty"
    val result = UserUtil.createUserWithName(firstName)

    assertThat(result, `is`(notNullValue()))
    assertThat<UUID>(result.id, `is`(notNullValue()))
    assertThat<String>(result.firstName, equalTo(firstName))
    assertThat<String>(result.lastName, equalTo(""))
    assertThat<String>(result.displayName, equalTo(firstName))
  }

  @Test
  fun createUserWithName_firstAndLastName() {
    val firstName = "Betty"
    val lastName = "White"
    val result = UserUtil.createUserWithName(firstName + " " + lastName)

    assertThat(result, `is`(notNullValue()))
    assertThat(result.id, `is`(notNullValue()))
    assertThat(result.firstName, equalTo(firstName))
    assertThat(result.lastName, equalTo(lastName))
  }

  @Test
  fun createUserWithName_middleOrMultipleLastNames() {
    val firstName = "Donnie"
    val lastName = "van Zant"

    val result = UserUtil.createUserWithName(firstName + " " + lastName)

    assertThat(result, `is`(notNullValue()))
    assertThat<UUID>(result.id, `is`(notNullValue()))
    assertThat<String>(result.firstName, equalTo(firstName))
    assertThat<String>(result.lastName, equalTo(lastName))
  }

  @Test(expected = IllegalArgumentException::class)
  fun createUserWithName_emptyName_throwsException() {
    UserUtil.createUserWithName("")
    fail("Should have thrown an exception")
  }

  @Test
  fun getDefaultUser_success() {
    val zeroUuid = "00000000-0000-0000-0000-000000000000"
    whenever(mockContext.getString(ArgumentMatchers.anyInt()))
        .thenReturn("Player 1")
        .thenReturn(zeroUuid)

    val result = UserUtil.getDefaultUser(mockContext)

    assertThat(result, `is`(notNullValue()))
    assertThat<String>(result.firstName, equalTo("Player"))
    assertThat<String>(result.lastName, equalTo("1"))
    assertThat<UUID>(result.id, equalTo(UUID.fromString("00000000-0000-0000-0000-000000000000")))
  }

}
