package net.cyberratt.disc.util

import android.text.Editable
import android.widget.EditText
import com.nhaarman.mockito_kotlin.whenever
import net.cyberratt.disc.KotlinTest
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.`is`
import org.junit.Test
import org.mockito.Mockito.mock

class TextUtilTest: KotlinTest() {

  @Test
  fun isPopulated_nullEditText_false() {
    val nullEditText: EditText? = null
    val mockEditText = mock(EditText::class.java)
    val mockEditable = mock(Editable::class.java)
    whenever(mockEditText.text).thenReturn(mockEditable)
    whenever(mockEditable.toString()).thenReturn("Stuff")
    assertThat(TextUtil.isPopulated(mockEditText, nullEditText), `is`(false))
  }

  @Test
  fun isPopulated_nullEditable_false() {
    val editText = mock(EditText::class.java)
    whenever(editText.text).thenReturn(null)

    assertThat(TextUtil.isPopulated(editText), `is`(false))
  }

  @Test
  fun isPopulated_emptyEditable_false() {
    val mockEditText = mock(EditText::class.java)
    val mockEditable = mock(Editable::class.java)
    val emptyString = ""

    whenever(mockEditText.text).thenReturn(mockEditable)
    whenever(mockEditable.toString()).thenReturn(emptyString)

    assertThat(TextUtil.isPopulated(mockEditText), `is`(false))
  }

  @Test
  fun isPopulated_editText_success() {
    val mockEditText = mock(EditText::class.java)
    val mockEditable = mock(Editable::class.java)

    whenever(mockEditText.text).thenReturn(mockEditable)
    whenever(mockEditable.toString()).thenReturn("Stuff")

    assertThat(TextUtil.isPopulated(mockEditText), `is`(true))
  }

  @Test
  fun isPopulated_nullString_false() {
    val nullString: String? = null
    assertThat(TextUtil.isPopulated(nullString), `is`(false))
  }

  @Test
  fun isPopulated_emptyString_false() {
    val emptyString = ""
    assertThat(TextUtil.isPopulated(emptyString), `is`(false))
  }

  @Test
  fun isPopulated_string_success() {
    val populatedString = "This string has stuff in it"
    assertThat(TextUtil.isPopulated(populatedString), `is`(true))
  }
}
