package net.cyberratt.disc.util

import net.cyberratt.disc.KotlinTest
import net.cyberratt.disc.db.model.User
import org.hamcrest.CoreMatchers.*
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test

class GameUtilTest: KotlinTest() {

  @Test
  fun createGameForUserList_success() {
    val playerOne = User(
        firstName = "Player",
        lastName = "One",
        email = "playerone@email.com")
    val playerTwo = User(
      firstName = "Player",
      lastName = "Two",
      email = "playertwo@email.com")

    val userList = listOf(playerOne, playerTwo)
    val result = GameUtil.createGameForUserList(userList)

    assertThat(result, `is`(notNullValue()))
    assertThat(result.scores.size, equalTo(userList.size))
    assertThat(result.scores.all { userList.contains(it.user) }, `is`(true))
  }

}
