package net.cyberratt.disc.util

import net.cyberratt.disc.KotlinTest
import org.hamcrest.Matchers.equalTo
import org.junit.Assert.assertThat
import org.junit.Test

class StringUtilTest : KotlinTest() {

  @Test
  fun splitName_twoNames() {
    val name = "Testy McTesterson"
    val (firstName, lastName) = StringUtil.splitNameString(name)
    assertThat<String>(firstName, equalTo("Testy"))
    assertThat<String>(lastName, equalTo("McTesterson"))
  }
  @Test
  fun splitName_oneName() {
    val name = "Testy"
    val (firstName, lastName) = StringUtil.splitNameString(name)
    assertThat<String>(firstName, equalTo("Testy"))
    assertThat<String>(lastName, equalTo(""))
  }
  @Test
  fun splitName_threeNames() {
    val name = "Testy Clarence McTesterson"
    val (firstName, lastName) = StringUtil.splitNameString(name)
    assertThat<String>(firstName, equalTo("Testy"))
    assertThat<String>(lastName, equalTo("Clarence McTesterson"))
  }

//  @Test
//  fun splitName_
}