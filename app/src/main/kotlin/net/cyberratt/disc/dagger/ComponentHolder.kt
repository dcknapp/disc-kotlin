package net.cyberratt.disc.dagger

class ComponentHolder private constructor() {

  private var coreComponent: CoreComponent? = null

  companion object {

    private val instance = ComponentHolder()

    fun getCore() = instance.coreComponent
    fun setCore(component: CoreComponent) {
      instance.coreComponent = component
    }
  }
}
