package net.cyberratt.disc.dagger.modules

import android.content.Context
import android.content.SharedPreferences
import com.squareup.moshi.Moshi
import dagger.Module
import dagger.Provides
import net.cyberratt.disc.util.Preferences
import javax.inject.Singleton

@Module(includes = arrayOf(ContextModule::class, MoshiModule::class))
class PreferencesModule {

  @Provides
  @Singleton
  fun providesDefaultSharedPreferences(context: Context): SharedPreferences {
    return context.getSharedPreferences(Preferences.Keys.DEFAULT_SHARED_PREFS, Context.MODE_PRIVATE)
  }

  @Provides
  @Singleton
  fun providePreferences(context: Context, moshi: Moshi, sharedPrefs: SharedPreferences): Preferences {
    return Preferences(sharedPrefs, moshi, context)
  }

}