package net.cyberratt.disc.dagger


import dagger.Component
import net.cyberratt.disc.dagger.modules.PreferencesModule
import net.cyberratt.disc.dagger.modules.RestModule
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(
    PreferencesModule::class,
    RestModule::class))
interface CoreComponent : BaseComponent
