package net.cyberratt.disc.dagger.modules

import com.squareup.moshi.KotlinJsonAdapterFactory
import com.squareup.moshi.Moshi
import dagger.Module
import dagger.Provides
import net.cyberratt.disc.network.typeadapter.JodaDateTimeMoshiAdapter
import net.cyberratt.disc.network.typeadapter.UuidMoshiAdapter
import javax.inject.Singleton

@Module
class MoshiModule {

  @Provides
  @Singleton
  fun provideMoshi(): Moshi =
      Moshi.Builder()
          .add(KotlinJsonAdapterFactory())
          .add(JodaDateTimeMoshiAdapter())
          .add(UuidMoshiAdapter())
          .build()

}
