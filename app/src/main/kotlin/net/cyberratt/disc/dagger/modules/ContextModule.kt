package net.cyberratt.disc.dagger.modules

import android.content.Context
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ContextModule(internal val context: Context) {

  @Provides
  @Singleton
  fun provideContext() = context
}
