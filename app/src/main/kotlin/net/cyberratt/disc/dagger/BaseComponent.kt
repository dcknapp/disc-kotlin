package net.cyberratt.disc.dagger

import net.cyberratt.disc.ui.activity.*
import net.cyberratt.disc.ui.fragment.HoleFragment
import net.cyberratt.disc.ui.fragment.HoleReviewFragment
import net.cyberratt.disc.ui.fragment.LoginFragment
import net.cyberratt.disc.ui.fragment.ProfileLandingPageFragment
import net.cyberratt.disc.ui.view.PlayerListRecyclerViewAdapter
import net.cyberratt.disc.ui.view.ScorecardCardView

interface BaseComponent {

  fun inject(gameReviewActivity: GameReviewActivity)
  fun inject(gameSetupActivity: GameSetupActivity)
  fun inject(mainActivity: MainActivity)
  fun inject(scoreCardActivity: ScoreCardActivity)

  fun inject(holeFragment: HoleFragment)
  fun inject(holeReviewFragment: HoleReviewFragment)
  fun inject(loginFragment: LoginFragment)
  fun inject(profileLandingPageFragment: ProfileLandingPageFragment)

  fun inject(playerListRecyclerViewAdapter: PlayerListRecyclerViewAdapter)
  fun inject(scorecardCardView: ScorecardCardView)
  fun inject(signUpActivity: SignUpActivity)

}