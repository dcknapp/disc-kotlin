package net.cyberratt.disc.dagger.modules

import android.content.Context
import com.squareup.moshi.Moshi
import dagger.Module
import dagger.Provides
import net.cyberratt.disc.R
import net.cyberratt.disc.network.DriverRestService
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Singleton

@Module(includes = arrayOf(ContextModule::class, MoshiModule::class))
class RestModule {

  @Singleton
  @Provides
  fun provideDriverRestClient(context: Context, moshi: Moshi): DriverRestService {
    return Retrofit.Builder()
        .baseUrl(context.getString(R.string.server_url_root))
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .addConverterFactory(MoshiConverterFactory.create(moshi))
        .build()
        .create(DriverRestService::class.java)
  }

}
