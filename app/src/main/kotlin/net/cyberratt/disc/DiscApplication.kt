package net.cyberratt.disc

import android.app.Application
import net.cyberratt.disc.dagger.ComponentHolder
import net.cyberratt.disc.dagger.CoreComponent
import net.cyberratt.disc.dagger.DaggerCoreComponent
import net.cyberratt.disc.dagger.modules.ContextModule
import timber.log.Timber


open class DiscApplication : Application() {

  override fun onCreate() {
    super.onCreate()
    setCoreComponent(buildComponent())
    Timber.plant(Timber.DebugTree())
  }

  private fun setCoreComponent(coreComponent: CoreComponent) {
    ComponentHolder.setCore(coreComponent)
  }

  open fun buildComponent(): CoreComponent {
    return DaggerCoreComponent.builder()
        .contextModule(ContextModule(this))
        .build()
  }
}
