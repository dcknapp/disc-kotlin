package net.cyberratt.disc.db.model

import android.os.Parcel
import android.os.Parcelable
import java.util.*

class Location (
    val id: UUID = UUID.randomUUID(),
    var name: String? = null,
    var latitude: Double = 0.toDouble(),
    var longitude: Double = 0.toDouble(),
    var streetAddress: String? = null,
    var city: String? = null,
    var state: String? = null,
    var zipCode: Int = 0,
    var country: String? = null
) : Parcelable {
  constructor(parcel: Parcel) : this(
      UUID.fromString(parcel.readString()),
      parcel.readString(),
      parcel.readDouble(),
      parcel.readDouble(),
      parcel.readString(),
      parcel.readString(),
      parcel.readString(),
      parcel.readInt(),
      parcel.readString())

  override fun writeToParcel(parcel: Parcel, flags: Int) {
    parcel.writeString(id.toString())
    parcel.writeString(name)
    parcel.writeDouble(latitude)
    parcel.writeDouble(longitude)
    parcel.writeString(streetAddress)
    parcel.writeString(city)
    parcel.writeString(state)
    parcel.writeInt(zipCode)
    parcel.writeString(country)
  }
  override fun describeContents() = 0

  companion object CREATOR : Parcelable.Creator<Location> {

    override fun createFromParcel(parcel: Parcel): Location {
      return Location(parcel)
    }
    override fun newArray(size: Int): Array<Location?> {
      return arrayOfNulls(size)
    }

  }

  fun printZipCode() = String.format("%05d", zipCode)
}
