package net.cyberratt.disc.db.model

import android.os.Parcel
import android.os.Parcelable
import java.util.*

class Course(
    val id: UUID = UUID.randomUUID(),
    var name: String? = null,
    var location: Location? = null,
    var description: String? = null,
    var numberOfHoles: Int = 18,
    var notes: List<Note> = ArrayList(),
    var reviews: List<Review> = ArrayList()
) : Parcelable {
  constructor(parcel: Parcel) : this(
      UUID.fromString(parcel.readString()),
      parcel.readString(),
      parcel.readParcelable<Location>(null),
      parcel.readString(),
      parcel.readInt(),
      parcel.readArrayList(null) as List<Note>,
      parcel.readArrayList(null) as List<Review>)

  override fun writeToParcel(parcel: Parcel, flags: Int) {
    parcel.writeString(id.toString())
    parcel.writeString(name)
    parcel.writeParcelable(location, 0)
    parcel.writeString(description)
    parcel.writeInt(numberOfHoles)
    parcel.writeTypedList(notes)
    parcel.writeTypedList(reviews)
  }

  override fun describeContents(): Int {
    return 0
  }

  companion object CREATOR : Parcelable.Creator<Course> {
    override fun createFromParcel(parcel: Parcel): Course {
      return Course(parcel)
    }

    override fun newArray(size: Int): Array<Course?> {
      return arrayOfNulls(size)
    }
  }
}


