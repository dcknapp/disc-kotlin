package net.cyberratt.disc.db.model

import android.os.Parcel
import android.os.Parcelable
import android.util.SparseIntArray
import net.cyberratt.disc.util.ScoreUtil
import java.util.*

class Score(
    val id: UUID = UUID.randomUUID(),
    var user: User? = null,
    var scoreString:String = ""
) : Parcelable {

  fun setScores(holeScores: SparseIntArray) {
    scoreString = ScoreUtil.toDelimitedString(holeScores)
  }

  val holeNumberedScoreMap: SparseIntArray
    get() = ScoreUtil.toHoleNumberedMap(scoreString)

  val totalScore: Int
    get() = ScoreUtil.getTotalScore(scoreString)

  constructor(parcel: Parcel) : this(
      UUID.fromString(parcel.readString()),
      parcel.readParcelable(User::class.java.classLoader),
      parcel.readString())

  override fun writeToParcel(parcel: Parcel, flags: Int) {
    parcel.writeString(id.toString())
    parcel.writeParcelable(user, flags)
    parcel.writeString(scoreString)
  }

  override fun describeContents(): Int {
    return 0
  }

  companion object CREATOR : Parcelable.Creator<Score> {

    override fun createFromParcel(parcel: Parcel): Score {
      return Score(parcel)
    }
    override fun newArray(size: Int): Array<Score?> {
      return arrayOfNulls(size)
    }

  }

  enum class ScoreAction(private val modifierValue: Int) {
    INCREASE_SCORE(1),
    DECREASE_SCORE(-1);

    fun modifierValue(): Int {
      return modifierValue
    }
  }
}
