package net.cyberratt.disc.db.model

import android.os.Parcel
import android.os.Parcelable
import java.util.*

class User(
    val id: UUID = UUID.randomUUID(),
    var firstName: String? = null,
    var lastName: String? = null,
    var email: String? = null,
    var nickname: String? = null,
    var location: Location? = null,
    var password: String? = null
) : Parcelable{
  val displayName: String?
    get() {
      return if (firstName != null) {
        if (lastName != null && !lastName!!.isEmpty()) {
          "${firstName!!} ${lastName!![0]}".trim()
        } else {
          "$firstName"
        }
      } else {
        ""
      }

    }

  constructor(parcel: Parcel) : this(
      UUID.fromString(parcel.readString()),
      parcel.readString(),
      parcel.readString(),
      parcel.readString(),
      parcel.readString(),
      parcel.readParcelable(Location::class.java.classLoader),
      parcel.readString())

  override fun writeToParcel(parcel: Parcel, flags: Int) {
    parcel.writeString(id.toString())
    parcel.writeString(firstName)
    parcel.writeString(lastName)
    parcel.writeString(email)
    parcel.writeString(nickname)
    parcel.writeParcelable(location, flags)
    parcel.writeString(password)
  }

  override fun describeContents(): Int {
    return 0
  }

  companion object CREATOR : Parcelable.Creator<User> {
    override fun createFromParcel(parcel: Parcel): User {
      return User(parcel)
    }

    override fun newArray(size: Int): Array<User?> {
      return arrayOfNulls(size)
    }
  }
}
