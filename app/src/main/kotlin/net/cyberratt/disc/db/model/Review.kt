package net.cyberratt.disc.db.model

import android.os.Parcel
import android.os.Parcelable
import org.joda.time.DateTime
import java.util.*

class Review(
    val id: UUID,
    var author: User?,
    var content: String?,
    var course: Course?,
    var rating: Int?,
    var gameDate: DateTime?,
    var reviewRating: Int?
) : Parcelable {
  constructor(parcel: Parcel) : this(
      UUID.fromString(parcel.readString()),
      parcel.readParcelable(User::class.java.classLoader),
      parcel.readString(),
      parcel.readParcelable(Course::class.java.classLoader),
      parcel.readValue(Int::class.java.classLoader) as? Int,
      DateTime(parcel.readLong()),
      parcel.readValue(Int::class.java.classLoader) as? Int)

  override fun writeToParcel(parcel: Parcel, flags: Int) {
    parcel.writeString(id.toString())
    parcel.writeParcelable(author, flags)
    parcel.writeString(content)
    parcel.writeParcelable(course, flags)
    parcel.writeValue(rating)
    parcel.writeLong(gameDate?.toDate()?.time!!)
    parcel.writeValue(reviewRating)
  }

  override fun describeContents(): Int {
    return 0
  }

  companion object CREATOR : Parcelable.Creator<Review> {
    override fun createFromParcel(parcel: Parcel): Review {
      return Review(parcel)
    }

    override fun newArray(size: Int): Array<Review?> {
      return arrayOfNulls(size)
    }
  }
}

