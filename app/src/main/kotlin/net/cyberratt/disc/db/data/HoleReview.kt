package net.cyberratt.disc.db.data

import android.os.Parcel
import android.os.Parcelable

data class HoleReview(val playerName: String, val holeScore: Int, val totalScore: Int) : Parcelable {
  constructor(parcel: Parcel) : this(
      parcel.readString(),
      parcel.readInt(),
      parcel.readInt())

  override fun writeToParcel(parcel: Parcel, flags: Int) {
    parcel.writeString(playerName)
    parcel.writeInt(holeScore)
    parcel.writeInt(totalScore)
  }

  override fun describeContents(): Int {
    return 0
  }

  companion object CREATOR : Parcelable.Creator<HoleReview> {
    override fun createFromParcel(parcel: Parcel): HoleReview {
      return HoleReview(parcel)
    }

    override fun newArray(size: Int): Array<HoleReview?> {
      return arrayOfNulls(size)
    }
  }
}