package net.cyberratt.disc.db.model

import android.os.Parcel
import android.os.Parcelable
import java.util.*

class Note (
    val id: UUID? = null,
    var author: User? = null,
    var content: String? = null
) : Parcelable {
  constructor(parcel: Parcel) : this(
      UUID.fromString(parcel.readString()),
      parcel.readParcelable<User>(null),
      parcel.readString())

  override fun writeToParcel(parcel: Parcel, flags: Int) {
    parcel.writeString(id.toString())
    parcel.writeParcelable(author, 0)
    parcel.writeString(content)
  }

  override fun describeContents(): Int {
    return 0
  }

  companion object CREATOR : Parcelable.Creator<Note> {
    override fun createFromParcel(parcel: Parcel): Note {
      return Note(parcel)
    }

    override fun newArray(size: Int): Array<Note?> {
      return arrayOfNulls(size)
    }
  }
}
