package net.cyberratt.disc.db.model

import android.os.Parcel
import android.os.Parcelable
import org.joda.time.DateTime
import java.util.UUID
import kotlin.collections.ArrayList
import kotlin.collections.List
import kotlin.collections.toList

class Game(
    val id: UUID = UUID.randomUUID(),
    var course: Course = Course(),
    var gameStart: DateTime = DateTime.now(),
    var gameEnd: DateTime? = null,
    val scores: List<Score> = ArrayList()
) : Parcelable {
  constructor(parcel: Parcel) : this(
      UUID.fromString(parcel.readString()),
      parcel.readParcelable<Course>(null),
      DateTime(parcel.readLong()),
      DateTime(parcel.readLong()),
      parcel.readArrayList(null) as ArrayList<Score>)

  fun addScore(score: Score) {
    (scores as ArrayList).add(score)
  }
  override fun writeToParcel(parcel: Parcel, flags: Int) {
    parcel.writeString(id.toString())
    parcel.writeParcelable(course, 0)
    parcel.writeLong(gameStart.toDate().time)
    parcel.writeLong(gameEnd?.toDate()?.time!!)
    parcel.writeTypedList(scores.toList())
  }

  override fun describeContents(): Int {
    return 0
  }

  companion object CREATOR : Parcelable.Creator<Game> {
    override fun createFromParcel(parcel: Parcel): Game {
      return Game(parcel)
    }

    override fun newArray(size: Int): Array<Game?> {
      return arrayOfNulls(size)
    }
  }
}

