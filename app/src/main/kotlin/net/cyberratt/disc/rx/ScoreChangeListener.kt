package net.cyberratt.disc.rx

import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject
import javax.inject.Singleton

@Singleton class ScoreChangeListener @Inject constructor() {

  private val relay: PublishSubject<Unit> = PublishSubject.create()

  fun relay(): Observable<Unit> = relay
  fun update() = relay.onNext(Unit)
}