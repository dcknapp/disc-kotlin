package net.cyberratt.disc.network.request

import java.util.*

data class FriendRequest(var userId: UUID?, var friendId: UUID?)
