package net.cyberratt.disc.network.typeadapter

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import java.util.*

class UuidMoshiAdapter {
  @FromJson fun fromJson(dateString: String): UUID? {
    return UUID.fromString(dateString)
  }

  @ToJson fun toJson(uuid: UUID?): String? {
    return uuid.toString()
  }
}