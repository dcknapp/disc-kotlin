package net.cyberratt.disc.network.request

data class LoginRequest(var email: String?, val password: String?)