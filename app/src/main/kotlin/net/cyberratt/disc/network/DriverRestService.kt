package net.cyberratt.disc.network

import io.reactivex.Observable
import net.cyberratt.disc.db.model.Course
import net.cyberratt.disc.db.model.Game
import net.cyberratt.disc.db.model.User
import net.cyberratt.disc.network.request.FriendRequest
import net.cyberratt.disc.network.request.LoginRequest
import retrofit2.http.*


interface DriverRestService {

  @GET("users")
  fun users(): Observable<Collection<User>>

  @POST("users")
  fun createUser(@Body user: User): Observable<User>

  @GET("users/{id}")
  fun getUserById(@Path("id") id: String): Observable<User>

  @PUT("users/{id}")
  fun updateUser(@Path("id") id: String, @Body user: User): Observable<Void>

  @POST("users/login")
  fun login(@Body loginRequest: LoginRequest): Observable<User>

  @POST("users")
  fun addFriend(@Body friendRequest: FriendRequest): Observable<Void>

  @GET("courses")
  fun courses(): Observable<List<Course>>

  @POST("courses")
  fun saveCourse(@Body course: Course): Observable<Course>

  @GET("games/{id}")
  fun getCourse(@Path("id") id: String): Observable<Course>

  @DELETE("games/{id}")
  fun deleteCourse(@Path("id") id: String): Observable<Void>

  @GET("games")
  fun games(): Observable<List<Game>>

  @POST("games")
  fun saveGame(@Body game: Game?): Observable<Game>

  @GET("games/{id}")
  fun getGame(@Path("id") id: String): Observable<Game>

  @GET("games/{id}")
  fun deleteGame(@Path("id") id: String): Observable<Void>
}
