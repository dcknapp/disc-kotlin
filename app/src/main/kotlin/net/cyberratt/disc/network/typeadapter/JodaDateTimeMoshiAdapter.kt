package net.cyberratt.disc.network.typeadapter

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import org.joda.time.DateTime
import org.joda.time.format.ISODateTimeFormat

class JodaDateTimeMoshiAdapter {
  @FromJson fun fromJson(dateString: String): DateTime? {
    return DateTime(dateString)
  }

  @ToJson fun toJson(date: DateTime?): String? {
    return date?.toString(ISODateTimeFormat.dateTime())
  }
}
