package net.cyberratt.disc.ui.view


import android.content.Context
import android.support.v7.widget.CardView
import android.util.AttributeSet
import kotlinx.android.synthetic.main.scorecard_player_cardview.view.*
import net.cyberratt.disc.R
import net.cyberratt.disc.dagger.ComponentHolder
import net.cyberratt.disc.db.model.Score
import net.cyberratt.disc.db.model.User
import net.cyberratt.disc.rx.ScoreChangeListener
import net.cyberratt.disc.util.ScoreUtil
import javax.inject.Inject

class ScorecardCardView : CardView {

  companion object {
    val TAG: String = ScorecardCardView::class.java.simpleName
  }

  private lateinit var player: User
  private lateinit var scoreProvider: ScoreProvider
  private lateinit var playerCardHolder: PlayerCardHolder

  @Inject lateinit var scoreChangeListener: ScoreChangeListener

  constructor(context: Context) : super(context)
  constructor(context: Context, attrs: AttributeSet) : super(context, attrs)
  constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

  fun init() {
    ComponentHolder.getCore()?.inject(this)
    card_plus_stroke_button.setOnClickListener { processScoreAction(Score.ScoreAction.INCREASE_SCORE) }
    card_minus_stroke_button.setOnClickListener { processScoreAction(Score.ScoreAction.DECREASE_SCORE) }
  }

  private fun processScoreAction(action: Score.ScoreAction) {
    val score = ScoreUtil.getScoreForUser(scoreProvider.scores, player)
    val holeNumber = playerCardHolder.cardHoleNumber

    modifyScore(score, holeNumber, action)
    setScores(score, playerCardHolder.cardHoleNumber)

    scoreChangeListener.update()
  }

  fun populate(score: Score, holeNumber: Int) {
    id = Math.abs(score.user!!.id.hashCode())
    player = score.user!!
    card_player_name_textview.text = score.user?.displayName
    setScores(score, holeNumber)
  }

  private fun setScores(score: Score, holeNumber: Int) {
    val scoreString = score.scoreString
    card_back_half_score_textview.text = ScoreUtil.getBackHalfScore(scoreString).toString()
    card_front_half_score_textview.text = ScoreUtil.getFrontHalfScore(scoreString).toString()
    card_total_score_textview.text = ScoreUtil.getTotalScore(scoreString).toString()
    card_hole_total_textview.text = ScoreUtil.getScoreForHole(scoreString, holeNumber).toString()
    updateScoreDifference()
  }

  private fun modifyScore(score: Score, holeNumber: Int, scoreAction: Score.ScoreAction) {
    if (!(card_hole_total_textview.text.toString() == "0"
        && scoreAction === Score.ScoreAction.DECREASE_SCORE)) {

      val scoreMap = ScoreUtil.toHoleNumberedMap(score.scoreString)
      val holeScore = scoreMap[holeNumber]

      scoreMap.put(holeNumber, holeScore + scoreAction.modifierValue())

      score.scoreString = ScoreUtil.toDelimitedString(scoreMap)
    }
  }

  private fun updateScoreDifference() {
    val diff = ScoreUtil.calculateScoreDifference(scoreProvider.scores, player)

    card_score_difference_textview.text =
        if (diff > 0) {
          "${context.getString(R.string.plus)}$diff"
        } else {
          diff.toString()
        }
  }

  fun setScoreProvider(scoreProvider: ScoreProvider) {
    this.scoreProvider = scoreProvider
  }

  fun setPlayerCardHolder(playerCardHolder: PlayerCardHolder) {
    this.playerCardHolder = playerCardHolder
  }

  interface ScoreProvider {
    val scores: List<Score>
  }

  interface PlayerCardHolder {
    val cardHoleNumber: Int
  }

  interface ScoreChangedEventHandler {
    fun handleScoreChangedEvent()
  }
}
