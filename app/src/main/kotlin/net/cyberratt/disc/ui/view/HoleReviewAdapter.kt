package net.cyberratt.disc.ui.view

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.card_hole_review.view.*
import net.cyberratt.disc.R
import net.cyberratt.disc.db.data.HoleReview

class HoleReviewAdapter(private val holeReviews: List<HoleReview>)
  : RecyclerView.Adapter<HoleReviewAdapter.HoleReviewViewHolder>() {

  override fun getItemCount() = holeReviews.size

  override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): HoleReviewViewHolder {
    val view = LayoutInflater
        .from(parent?.context)
        .inflate(R.layout.card_hole_review, parent, false)

    return HoleReviewViewHolder(view)
  }

  override fun onBindViewHolder(holder: HoleReviewViewHolder, position: Int) {
    val review = holeReviews[position]
    holder.itemView.player_name.text = review.playerName
    holder.itemView.hole_score.text = review.holeScore.toString()
    holder.itemView.total_score.text = review.totalScore.toString()
  }

  class HoleReviewViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}