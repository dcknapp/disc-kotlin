package net.cyberratt.disc.ui.fragment

import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.annotation.VisibleForTesting
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import butterknife.ButterKnife
import butterknife.OnClick
import butterknife.Unbinder
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_login.*
import net.cyberratt.disc.R
import net.cyberratt.disc.dagger.ComponentHolder
import net.cyberratt.disc.db.model.User
import net.cyberratt.disc.network.DriverRestService
import net.cyberratt.disc.network.request.LoginRequest
import net.cyberratt.disc.ui.activity.SignUpActivity
import net.cyberratt.disc.util.Preferences
import net.cyberratt.disc.util.ToastUtil
import timber.log.Timber
import javax.inject.Inject

class LoginFragment : Fragment() {

  @Inject
  internal lateinit var restService: DriverRestService

  @Inject
  internal lateinit var preferences: Preferences

  @VisibleForTesting
  internal var loggingInDialog: ProgressDialog? = null
  private var fragmentInteractionListener: OnFragmentInteractionListener? = null
  private var unbinder: Unbinder? = null

  override fun onAttach(context: Context?) {
    super.onAttach(context)
    if (context is OnFragmentInteractionListener) {
      fragmentInteractionListener = context
    } else {
      throw RuntimeException(context!!.toString() + " must implement OnFragmentInteractionListener")
    }
  }

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    ComponentHolder.getCore()?.inject(this)
  }

  override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
    val view = inflater!!.inflate(R.layout.fragment_login, container, false)
    unbinder = ButterKnife.bind(this, view)
    return view
  }

  override fun onDetach() {
    super.onDetach()
    fragmentInteractionListener = null
  }

  override fun onDestroyView() {
    super.onDestroyView()
    unbinder?.unbind()
  }

  @OnClick(R.id.login_clear_button)
  fun clearInputs() {
    main_email_edit_text.setText(R.string.empty_string)
    main_password_edit_text.setText(R.string.empty_string)
  }

  @OnClick(R.id.login_submit_button)
  fun login() {
    val loginRequest = LoginRequest(main_email_edit_text.text.toString(), main_password_edit_text.text.toString())
    loggingInDialog = ProgressDialog.show(activity, "Please wait", "Attempting login...")

    restService.login(loginRequest)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe( { handleLoginSuccess(it) },
                    { handleLoginFailure(it) })
  }

  private fun handleLoginSuccess(user: User) {
    ToastUtil.displayToast(activity, "Logged in " + user.displayName!!)
    Timber.d(TAG, "Updating logged in user")

    preferences.logOutCurrentUser()
    preferences.loggedInUser = user

    fragmentInteractionListener!!.updateNavigationDrawer()
    fragmentInteractionListener!!.updateLandingPageFragment()

    finishDialog()
  }

  private fun handleLoginFailure(error: Throwable) {
    Timber.e(TAG, "Error subscribing to login call", error)
    ToastUtil.displayToast(activity, "Something went wrong during login.")
    finishDialog()
  }

  private fun finishDialog() {
    hideKeyboard()
    loggingInDialog?.dismiss()
  }

  private fun hideKeyboard() {
    val view = activity.currentFocus
    if (view != null) {
      val imm = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
      imm.hideSoftInputFromWindow(view.windowToken, 0)
    }
  }

  @OnClick(R.id.login_signup_button)
  fun launchSignUpActivity() {
    startActivity(Intent(activity, SignUpActivity::class.java))
  }

  interface OnFragmentInteractionListener {

    fun updateNavigationDrawer()
    fun updateLandingPageFragment()
  }

  companion object {

    val TAG: String = LoginFragment::class.java.simpleName

    fun newInstance(): LoginFragment {
      return LoginFragment()
    }
  }
}
