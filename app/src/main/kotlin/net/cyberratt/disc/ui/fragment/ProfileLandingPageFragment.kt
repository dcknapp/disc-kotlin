package net.cyberratt.disc.ui.fragment


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_profile_landing_page.view.*
import net.cyberratt.disc.R
import net.cyberratt.disc.dagger.ComponentHolder
import net.cyberratt.disc.util.Preferences
import javax.inject.Inject

class ProfileLandingPageFragment : Fragment() {

  @Inject lateinit var preferences: Preferences

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    ComponentHolder.getCore()?.inject(this)
  }

  override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                            savedInstanceState: Bundle?): View? {
    val view = inflater!!.inflate(R.layout.fragment_profile_landing_page, container, false)
    setGreeting()

    return view
  }

  private fun setGreeting() {
    view?.greeting_textview?.text =
        String.format(activity.getString(R.string.greeting), preferences.loggedInOrDefaultUser.displayName)
  }

  companion object {

    val TAG: String = ProfileLandingPageFragment::class.java.simpleName

    fun newInstance(): ProfileLandingPageFragment {
      return ProfileLandingPageFragment()
    }
  }
}
