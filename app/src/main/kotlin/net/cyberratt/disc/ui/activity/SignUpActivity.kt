package net.cyberratt.disc.ui.activity

import android.app.ProgressDialog
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import butterknife.ButterKnife
import butterknife.OnClick
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_sign_up.*
import net.cyberratt.disc.R
import net.cyberratt.disc.dagger.ComponentHolder
import net.cyberratt.disc.db.model.User
import net.cyberratt.disc.network.DriverRestService
import net.cyberratt.disc.network.request.LoginRequest
import net.cyberratt.disc.util.Preferences
import net.cyberratt.disc.util.TextUtil
import net.cyberratt.disc.util.ToastUtil
import timber.log.Timber
import javax.inject.Inject

class SignUpActivity : AppCompatActivity() {

  internal var createAccountDialog: ProgressDialog? = null
  internal var loggingInDialog: ProgressDialog? = null

  @Inject lateinit var restService: DriverRestService
  @Inject lateinit var preferences: Preferences

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_sign_up)

    ButterKnife.bind(this@SignUpActivity)
    ComponentHolder.getCore()?.inject(this)
  }

  @OnClick(R.id.submit_signup_button)
  fun submitSignUpForm() {
    if (isFormCompleted()) {
      Timber.d(TAG, "Submitting signup form")
      val user = buildUserFromForm()
      createAccountDialog = ProgressDialog.show(this, "Creating account", "Please wait...")

      restService.createUser(user)
          .subscribeOn(Schedulers.io())
          .observeOn(AndroidSchedulers.mainThread())
          .subscribeBy(
              onNext = { logInUser(it) },
              onError = { handleCreateAccountFailure(it) },
              onComplete = {
                Timber.d(TAG, "Completed signup call")
                createAccountDialog?.dismiss() })
    } else {
      ToastUtil.displayToast(this, getString(R.string.form_incomplete_message))
    }
  }

  private fun isFormCompleted(): Boolean {
    return TextUtil.isPopulated(
        sign_up_first_name_input,
        sign_up_last_name_input,
        sign_up_email_input,
        sign_up_password_input,
        sign_up_password_confirmation_input)
  }

  private fun buildUserFromForm(): User {
    return User(
        firstName = sign_up_first_name_input.text.toString(),
        lastName = sign_up_last_name_input.text.toString(),
        email = sign_up_email_input.text.toString(),
        password = sign_up_password_input.text.toString())
  }

  private fun handleCreateAccountFailure(error: Throwable) {
    Timber.e(TAG, "Error creating account: ", error)
    createAccountDialog?.dismiss()
    AlertDialog.Builder(this)
        .setTitle("Error")
        .setMessage("There was an error creating your account")
        .setPositiveButton("OK") { dialog, _ -> dialog.dismiss() }
        .show()
  }

  private fun logInUser(user: User) {
    Timber.d(TAG, "Logging in user")
    loggingInDialog = ProgressDialog.show(this, "User created", "Logging you in...")

    restService.login(getLoginRequest())
        .subscribeOn(Schedulers.newThread())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribeBy(
            onNext = { handleLoginSuccess(user) },
            onError = { showLoginErrorDialog(it) },
            onComplete = { loggingInDialog?.dismiss() })

  }

  private fun getLoginRequest() = LoginRequest(sign_up_email_input.text.toString(), sign_up_password_input.text.toString())

  private fun handleLoginSuccess(user: User) {
    Timber.d(TAG, "Handling login success")
    preferences.loggedInUser = user
    closeActivity()
  }

  private fun showLoginErrorDialog(error: Throwable) {
    Timber.d(TAG, "Handling login failure: " + error.message)
    AlertDialog.Builder(this)
        .setTitle("Error")
        .setMessage("User was created but errors occurred while logging in. Please log in manually later.")
        .setPositiveButton("OK") { dialog, _ -> dialog.dismiss() }
        .setOnDismissListener { _ -> closeActivity() }
        .show()
  }

  @OnClick(R.id.cancel_signup_button)
  fun closeActivity() {
    loggingInDialog?.apply { if (isShowing) dismiss() }
    createAccountDialog?.apply { if (isShowing) dismiss() }
    finish()
  }

  companion object {

    val TAG: String = SignUpActivity::class.java.simpleName
  }
}
