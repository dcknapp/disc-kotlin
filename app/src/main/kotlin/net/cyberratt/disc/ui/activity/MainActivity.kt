package net.cyberratt.disc.ui.activity

import android.animation.AnimatorInflater
import android.animation.AnimatorSet
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.app.Fragment
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.view.MotionEvent
import android.view.View
import android.widget.TextView
import butterknife.ButterKnife
import butterknife.OnClick
import butterknife.OnTouch
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import net.cyberratt.disc.BuildConfig
import net.cyberratt.disc.R
import net.cyberratt.disc.dagger.ComponentHolder
import net.cyberratt.disc.ui.fragment.LoginFragment
import net.cyberratt.disc.ui.fragment.ProfileLandingPageFragment
import net.cyberratt.disc.util.Preferences
import net.cyberratt.disc.util.TAG
import net.cyberratt.disc.util.ToastUtil
import timber.log.Timber
import javax.inject.Inject

class MainActivity : AppCompatActivity(),
    NavigationView.OnNavigationItemSelectedListener, LoginFragment.OnFragmentInteractionListener {

  companion object {
    val LOGIN_FRAGMENT_TAG: String = ".FRAGMENT_LOGIN"
    val PROFILE_FRAGMENT_TAG: String = ".FRAGMENT_PROFILE"
  }

  @Inject lateinit var preferences: Preferences

  private val mButtonDownAnimation by lazy {
    AnimatorInflater.loadAnimator(this, R.animator.fab_bounce_down) as AnimatorSet
  }
  private val mButtonUpAnimation by lazy {
    AnimatorInflater.loadAnimator(this, R.animator.fab_bounce_up) as AnimatorSet
  }
  private val isUserLoggedIn: Boolean
    get() = preferences.loggedInUser != null

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_main)
    ComponentHolder.getCore()?.inject(this)
  }

  public override fun onStart() {
    super.onStart()

    setSupportActionBar(main_toolbar)

    ButterKnife.bind(this)

    setupNavigationDrawer()
    updateLandingPageFragment()
  }

  private fun setupNavigationDrawer() {
    syncNavigationDrawer()
    nav_view_main.setNavigationItemSelectedListener(this)
    updateNavigationDrawerHeader()
    updateNavigationDrawer()
  }

  private fun syncNavigationDrawer() {
    val toggle = ActionBarDrawerToggle(
        this, drawer_layout_main, main_toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
    drawer_layout_main.addDrawerListener(toggle)
    toggle.syncState()
  }

  private fun updateNavigationDrawerHeader() {
    val user = preferences.loggedInUser
    val header = nav_view_main.getHeaderView(0)

    if (user == null) {
      Timber.d(TAG(), "Updating nav header with log in message")
      header.findViewById<TextView>(R.id.nav_player_name_main).setText(R.string.create_account_message)
      header.findViewById<TextView>(R.id.nav_player_email_main).setText(R.string.empty_string)
    } else {
      Timber.d(TAG(), "Updating nav header info with user")
      header.findViewById<TextView>(R.id.nav_player_name_main).text = user.displayName
      header.findViewById<TextView>(R.id.nav_player_email_main).text = user.email
    }
  }

  @OnTouch(R.id.main_fab)
  fun onTouch(view: View, event: MotionEvent): Boolean {
    if (MotionEvent.ACTION_DOWN == event.action
        && !(mButtonUpAnimation.isRunning || mButtonUpAnimation.isStarted)) {
      mButtonDownAnimation.setTarget(view)
      mButtonDownAnimation.start()
    } else if (MotionEvent.ACTION_UP == event.action
      && !(mButtonDownAnimation.isRunning || mButtonDownAnimation.isStarted)) {
      mButtonUpAnimation.setTarget(view)
      mButtonUpAnimation.start()
    }

    return false
  }

  override fun onBackPressed() {
    if (drawer_layout_main.isDrawerOpen(GravityCompat.START)) {
      drawer_layout_main.closeDrawer(GravityCompat.START)
    } else {
      super.onBackPressed()
    }
  }

  override fun onNavigationItemSelected(item: MenuItem): Boolean {
    when(item.itemId) {
      R.id.nav_courses -> ToastUtil.displayToast(this, "Courses selected.")
      R.id.nav_games -> ToastUtil.displayToast(this, "Games selected.")
      R.id.nav_statistics -> ToastUtil.displayToast(this, "Statistics selected.")
      R.id.nav_leaderboards -> ToastUtil.displayToast(this, "Rankings selected.")
      R.id.nav_settings -> ToastUtil.displayToast(this, "Settings selected.")
      R.id.nav_profile -> ToastUtil.displayToast(this, "Profile selected.")
      R.id.nav_logout -> logout()
    }

    drawer_layout_main.closeDrawer(GravityCompat.START)
    return true
  }

  private fun logout() {
    preferences.logOutCurrentUser()
    updateNavigationDrawer()
    updateLandingPageFragment()
    ToastUtil.displayToast(this@MainActivity, "Logged out.")
  }

  @OnClick(R.id.main_fab)
  fun launchGameSetupActivity() {
    startActivity(Intent(this@MainActivity, GameSetupActivity::class.java))
  }

  override fun updateNavigationDrawer() {
    for (i in 0 until nav_view_main.menu.size()) {
      nav_view_main.menu.getItem(i).let {
        it.isEnabled = isUserLoggedIn || it.itemId == R.id.nav_settings
      }
    }
    updateNavigationDrawerHeader()
  }

  override fun updateLandingPageFragment() {
    val tag: String?
    val newFragment: Fragment?

    if (isUserLoggedIn) {
      Timber.w(TAG(), "Should display grey box")
      tag = "${BuildConfig.APPLICATION_ID}$LOGIN_FRAGMENT_TAG"
      newFragment = ProfileLandingPageFragment.newInstance()
    } else {
      Timber.w(TAG(), "Should display login")
      tag = "${BuildConfig.APPLICATION_ID}$PROFILE_FRAGMENT_TAG"
      newFragment = LoginFragment.newInstance()
    }

    replaceFragment(tag, newFragment)
  }

  private fun replaceFragment(tag: String, newFragment: Fragment) {
    with(supportFragmentManager) {
      beginTransaction()
          .replace(findFragmentByTag(tag)?.id ?: R.id.fragment_content, newFragment)
          .commit()
    }
  }

}
