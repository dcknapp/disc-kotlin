package net.cyberratt.disc.ui.activity

import android.os.Bundle
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import kotlinx.android.synthetic.main.activity_game_review.*
import net.cyberratt.disc.R
import net.cyberratt.disc.dagger.ComponentHolder
import net.cyberratt.disc.db.model.Score
import net.cyberratt.disc.ui.fragment.HoleReviewFragment
import net.cyberratt.disc.util.Preferences
import net.cyberratt.disc.util.ScoreUtil
import javax.inject.Inject

class GameReviewActivity : AppCompatActivity() {

  @Inject lateinit var preferences: Preferences
  private val scores: List<Score> by lazy {
    intent.getParcelableArrayListExtra<Score>("SCORES_PARCELABLE_KEY")
  }

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_game_review)
    setUpActionBar()

    ComponentHolder.getCore()?.inject(this)

    scorecard_viewpager.adapter = SectionsPagerAdapter(supportFragmentManager)
  }

  private fun setUpActionBar() {
    setSupportActionBar(toolbar)
    supportActionBar?.setDisplayHomeAsUpEnabled(true)
  }

  override fun onCreateOptionsMenu(menu: Menu): Boolean {
    menuInflater.inflate(R.menu.menu_game_review, menu)
    return true
  }

  override fun onOptionsItemSelected(item: MenuItem): Boolean {
    return when(item.itemId) {
      R.id.action_settings -> true
      else -> super.onOptionsItemSelected(item)
    }
  }

  inner class SectionsPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int) =
      HoleReviewFragment.newInstance(ScoreUtil.holeReviewForScores(scores, position))

    override fun getCount(): Int = preferences.currentCourse.numberOfHoles

    override fun getPageTitle(position: Int) = position.toString()
  }
}
