package net.cyberratt.disc.ui.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_game_review.*
import net.cyberratt.disc.R
import net.cyberratt.disc.db.data.HoleReview
import net.cyberratt.disc.ui.view.HoleReviewAdapter

class HoleReviewFragment : Fragment() {

  private val holeReviews: List<HoleReview> by lazy {
    arguments.getParcelableArrayList<HoleReview>(ARG_HOLE_REVIEWS)
  }

  override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                            savedInstanceState: Bundle?): View? {
    return inflater!!.inflate(R.layout.fragment_game_review, container, false)
  }

  override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
    recycler_view.layoutManager = LinearLayoutManager(context)
    recycler_view.adapter = HoleReviewAdapter(holeReviews)
  }

  companion object {

    private val ARG_HOLE_REVIEWS = "hole_reviews"

    fun newInstance(holeReviews: List<HoleReview>): HoleReviewFragment {
      return HoleReviewFragment().apply {
        arguments = Bundle().apply { putParcelableArrayList(ARG_HOLE_REVIEWS, holeReviews as ArrayList) }
      }
    }
  }
}