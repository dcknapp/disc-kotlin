package net.cyberratt.disc.ui.fragment.dialog


import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.app.AlertDialog
import net.cyberratt.disc.R

class CancelGameDialogFragment : DialogFragment() {

  override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
    val view = activity.layoutInflater.inflate(R.layout.fragment_cancel_game_dialog, null)

    return AlertDialog
        .Builder(activity)
        .setPositiveButton("End game") { dialogInterface, _ ->
          dialogInterface.dismiss()
          activity.finish()
        }
        .setNegativeButton("Keep playing") { dialogInterface, _ -> dialogInterface.dismiss() }
        .setView(view)
        .create()
  }
}
