package net.cyberratt.disc.ui.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_score_card.*
import kotlinx.android.synthetic.main.fragment_score_card.view.*
import net.cyberratt.disc.R
import net.cyberratt.disc.dagger.ComponentHolder
import net.cyberratt.disc.db.model.Game
import net.cyberratt.disc.rx.ScoreChangeListener
import net.cyberratt.disc.ui.activity.ScoreCardActivity
import net.cyberratt.disc.ui.view.ScoreCardRecyclerViewAdapter
import net.cyberratt.disc.ui.view.ScorecardCardView
import javax.inject.Inject

class HoleFragment : Fragment(),
    ScorecardCardView.PlayerCardHolder,
    ScorecardCardView.ScoreChangedEventHandler {

  private val holeNumber: Int by lazy { arguments.getInt(ARG_HOLE_NUMBER) }

  @Inject lateinit var scoreChangeListener: ScoreChangeListener

  override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
    return inflater?.inflate(R.layout.fragment_score_card, container, false) as LinearLayout?
  }

  override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)

    ComponentHolder.getCore()?.inject(this)

    setupRecyclerView()
    subscribeToScoreListener()
  }

  private fun setupRecyclerView() {
    scorecard_recyclerview.apply {
      setHasFixedSize(true)
      layoutManager = LinearLayoutManager(context)
      adapter = ScoreCardRecyclerViewAdapter(
          game.scores,
          holeNumber,
          activity as ScoreCardActivity,
          this@HoleFragment)
    }
  }

  private fun subscribeToScoreListener() {
    scoreChangeListener.relay()
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribeBy(onNext = { this.handleScoreChangedEvent() },
                     onError = {
                       throw RuntimeException("Something went wrong during score subscription")
                     })
  }

  private val game: Game
    get() = (activity as ScoreCardActivity).game

  override val cardHoleNumber: Int
    get() = holeNumber

  override fun handleScoreChangedEvent() {
    view?.scorecard_recyclerview?.adapter?.notifyDataSetChanged()
  }

  companion object {

    private val ARG_HOLE_NUMBER = "section_number"

    fun newInstance(holeNumber: Int): HoleFragment {
      return HoleFragment().apply {
        arguments = Bundle().apply { putInt(ARG_HOLE_NUMBER, holeNumber) }
      }
    }
  }
}