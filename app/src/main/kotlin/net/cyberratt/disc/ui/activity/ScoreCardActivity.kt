package net.cyberratt.disc.ui.activity


import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import com.squareup.moshi.Moshi
import kotlinx.android.synthetic.main.activity_score_card.*
import net.cyberratt.disc.R
import net.cyberratt.disc.dagger.ComponentHolder
import net.cyberratt.disc.db.model.Game
import net.cyberratt.disc.db.model.Score
import net.cyberratt.disc.network.DriverRestService
import net.cyberratt.disc.ui.fragment.HoleFragment
import net.cyberratt.disc.ui.fragment.dialog.CancelGameDialogFragment
import net.cyberratt.disc.ui.fragment.dialog.SaveGameDialogFragment
import net.cyberratt.disc.ui.view.ScorecardCardView
import net.cyberratt.disc.util.ToastUtil
import net.cyberratt.disc.util.fromJson
import org.joda.time.DateTime
import timber.log.Timber
import javax.inject.Inject

class ScoreCardActivity : AppCompatActivity(),
    ScorecardCardView.ScoreProvider,
    SaveGameDialogFragment.SaveGameCallback {

  val game: Game by lazy { moshi.fromJson<Game>(intent.getStringExtra(GAME_BUNDLE_KEY)) }

  private val sectionsPagerAdapter by lazy { SectionsPagerAdapter(supportFragmentManager) }
  private lateinit var savingGameDialog: ProgressDialog

  @Inject lateinit var restService: DriverRestService
  @Inject lateinit var moshi: Moshi

  override val scores: List<Score>
    get() = game.scores

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_score_card)
    ComponentHolder.getCore()?.inject(this)

    setGameInfo()
    setupSupportActionBar()

    scorecard_viewpager.adapter = sectionsPagerAdapter
    scorecard_viewpager.addOnPageChangeListener(object : ViewPager.SimpleOnPageChangeListener() {
      override fun onPageSelected(position: Int) {
        updateToolbarTitle(position)
        sectionsPagerAdapter.notifyDataSetChanged()
      }
    })
    updateToolbarTitle(scorecard_viewpager.currentItem)
  }

  private fun setupSupportActionBar() {
    setSupportActionBar(toolbar)
    supportActionBar?.setDisplayHomeAsUpEnabled(true)
  }

  override fun onResume() {
    super.onResume()
    ComponentHolder.getCore()?.inject(this)
  }

  override fun onBackPressed() {
    showCancelGameDialog()
  }

  private fun showCancelGameDialog() {
    CancelGameDialogFragment().show(supportFragmentManager, "CancelGameDialogFragment")
  }

  private fun setGameInfo() {
    game.gameStart = DateTime.now()
  }

  override fun onCreateOptionsMenu(menu: Menu): Boolean {
    menuInflater.inflate(R.menu.menu_score_card, menu)
    return true
  }

  override fun onOptionsItemSelected(item: MenuItem): Boolean {
    when(item.itemId) {
      R.id.save_game -> showSaveGameDialog()
      R.id.home -> showCancelGameDialog()
    }

    return true
  }

  private fun showSaveGameDialog() {
    SaveGameDialogFragment().apply {
      setSaveGameDialogInteractionListener(this@ScoreCardActivity)
      show(supportFragmentManager, "SaveGameDialogFragment")
    }
  }

  fun updateToolbarTitle(holeNumber: Int) {
    supportActionBar?.title = "Hole ${holeNumber + 1}"
  }

  override fun saveGame() {
    game.gameEnd = DateTime.now()

    savingGameDialog = ProgressDialog.show(this, "Please wait", "Saving game...")

//    restService.saveGame(game)
//        .subscribeOn(Schedulers.io())
//        .observeOn(AndroidSchedulers.mainThread())
//        .subscribeBy(onNext = { handleSaveGameSuccess() },
//                   onError = { handleSaveGameFailure(it) })
    handleSaveGameSuccess()
  }

  private fun handleSaveGameSuccess() {
    dismissDialog()
    startActivity(buildIntent())

    ToastUtil.displayToast(this, "Game saved successfully")
    finish()
  }

  private fun buildIntent(): Intent {
    return Intent(this, GameReviewActivity::class.java).apply {
      putExtra("SCORES_PARCELABLE_KEY", ArrayList(scores))
    }
  }

  private fun handleSaveGameFailure(error: Throwable) {
    dismissDialog()
    startActivity(buildIntent())
    ToastUtil.displayToast(this, "Error saving game")
    Timber.e(TAG, "Exception thrown: $error")
  }

  private fun dismissDialog() {
    if (savingGameDialog.isShowing) {
      savingGameDialog.dismiss()
    }
  }

  override fun onDestroy() {
    super.onDestroy()
    dismissDialog()
  }

  inner class SectionsPagerAdapter internal constructor(fm: FragmentManager) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
      return HoleFragment.newInstance(position + 1)
    }

    override fun getCount() = game.course.numberOfHoles

    override fun getPageTitle(position: Int) = (position + 1).toString()

    override fun getItemPosition(item: Any) = game.scores.indexOf(item)
  }

  companion object {

    val TAG: String = ScoreCardActivity::class.java.simpleName

    val GAME_BUNDLE_KEY = "net.cyberratt.disc.GAME_BUNDLE_KEY"
  }
}
