package net.cyberratt.disc.ui.fragment.dialog

import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.app.AlertDialog

import net.cyberratt.disc.R

class SaveGameDialogFragment : DialogFragment() {

  internal var saveGameDialogCallback: SaveGameCallback? = null

  override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
    val view = activity.layoutInflater.inflate(R.layout.fragment_save_game_dialog, null)

    return AlertDialog.Builder(activity).setPositiveButton("End game") { dialog, _ ->
      saveGameDialogCallback?.saveGame()
      dialog.dismiss()
      activity.finish()
    }
        .setNegativeButton("Keep playing") { dialog, _ -> dialog.dismiss() }
        .setView(view)
        .create()
  }

  fun setSaveGameDialogInteractionListener(listener: SaveGameCallback) {
    this.saveGameDialogCallback = listener
  }

  interface SaveGameCallback {
    fun saveGame()
  }
}
