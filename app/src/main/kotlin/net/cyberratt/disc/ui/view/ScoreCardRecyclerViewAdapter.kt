package net.cyberratt.disc.ui.view

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import net.cyberratt.disc.R
import net.cyberratt.disc.db.model.Score
import net.cyberratt.disc.ui.fragment.HoleFragment

class ScoreCardRecyclerViewAdapter(private val scores: List<Score>,
                                   private val holeNumber: Int,
                                   private val scoreProvider: ScorecardCardView.ScoreProvider,
                                   containingFragment: HoleFragment
) : RecyclerView.Adapter<ScoreCardRecyclerViewAdapter.CardViewHolder>() {

  private val cardHolder: ScorecardCardView.PlayerCardHolder = containingFragment

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CardViewHolder {
    val v = LayoutInflater.from(parent.context).inflate(R.layout.scorecard_player_cardview, parent, false)
    return CardViewHolder(v)
  }

  override fun onBindViewHolder(holder: CardViewHolder, position: Int) {
    holder.cardView.setScoreProvider(scoreProvider)
    holder.cardView.setPlayerCardHolder(cardHolder)
    holder.cardView.init()
    holder.cardView.populate(scores.elementAt(position), holeNumber)
  }

  override fun getItemCount(): Int {
    return scores.size
  }

  override fun onAttachedToRecyclerView(recyclerView: RecyclerView?) {
    super.onAttachedToRecyclerView(recyclerView)
  }

  class CardViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    var cardView: ScorecardCardView = itemView as ScorecardCardView

  }
}
