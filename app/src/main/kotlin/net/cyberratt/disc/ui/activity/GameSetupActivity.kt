package net.cyberratt.disc.ui.activity

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import butterknife.ButterKnife
import butterknife.OnClick
import com.squareup.moshi.Moshi
import kotlinx.android.synthetic.main.activity_game_setup.*
import net.cyberratt.disc.R
import net.cyberratt.disc.dagger.ComponentHolder
import net.cyberratt.disc.db.model.User
import net.cyberratt.disc.ui.view.PlayerListRecyclerViewAdapter
import net.cyberratt.disc.util.*
import java.util.*
import javax.inject.Inject

class GameSetupActivity : AppCompatActivity() {

  @Inject lateinit internal var preferences: Preferences
  @Inject lateinit internal var moshi: Moshi

  private var playerListAdapter: PlayerListRecyclerViewAdapter? = null
  private var players: MutableList<User> = ArrayList()

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_game_setup)

    ButterKnife.bind(this)
    ComponentHolder.getCore()?.inject(this)

    setupSupportActionBar()
    setupRecyclerView()
    setNextButtonClickListener()
  }

  private fun setupSupportActionBar() {
    setSupportActionBar(toolbar)
    supportActionBar?.apply {
      setDisplayShowTitleEnabled(false)
      setDisplayHomeAsUpEnabled(true)
    }
  }

  private fun setNextButtonClickListener() {
    new_game_next_arrow.setOnClickListener { _ ->
      saveCurrentCourse()
      val game = GameUtil.createGameForUserList(players)
      val scorecardIntent = Intent(this@GameSetupActivity, ScoreCardActivity::class.java)
      val bundle = Bundle()

      bundle.putString(ScoreCardActivity.GAME_BUNDLE_KEY, moshi.toJson(game))
      scorecardIntent.putExtras(bundle)
      startActivity(scorecardIntent)
      finish()
    }
  }

  private fun saveCurrentCourse() {
    preferences.currentCourse = getCourse()
  }

  //TODO ADD COURSES
  private fun getCourse() = CourseUtil.defaultCourse

  private fun setupRecyclerView() {
    val currentUser = preferences.loggedInOrDefaultUser
    players.add(currentUser)

    game_setup_recyclerview.setHasFixedSize(true)
    game_setup_recyclerview.layoutManager = LinearLayoutManager(this)
    playerListAdapter = PlayerListRecyclerViewAdapter(players, currentUser)
    game_setup_recyclerview.adapter = playerListAdapter
  }

  override fun onResume() {
    super.onResume()
    if (players.isEmpty()) {
      players.addAll(preferences.currentGameUsers ?: emptyList())
    }
  }

  override fun onPause() {
    super.onPause()
    if (!players.isEmpty()) {
      preferences.currentGameUsers = players
    }
  }

  @OnClick(R.id.add_player_plus_button)
  fun validateAndAddPlayerName() {
    if (add_player_edittext.length() > 0) {
      val name = add_player_edittext.text.toString().trim()

      if (name.contains(",")) {
        ToastUtil.displayToast(this, "Name cannot contain a comma")
      } else {
        players.add(UserUtil.createUserWithName(name))
        add_player_edittext.setText("")
        playerListAdapter?.notifyDataSetChanged()
      }
    }
  }

  companion object {

    val TAG: String = GameSetupActivity::class.java.simpleName
  }
}
