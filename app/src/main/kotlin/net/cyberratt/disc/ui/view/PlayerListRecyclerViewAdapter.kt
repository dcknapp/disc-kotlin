package net.cyberratt.disc.ui.view

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import kotlinx.android.synthetic.main.new_game_recyclerview_entry.view.*
import net.cyberratt.disc.R
import net.cyberratt.disc.db.model.User

class PlayerListRecyclerViewAdapter(private val players: MutableCollection<User>,
                                    private val gameOwner: User)
  : RecyclerView.Adapter<PlayerListRecyclerViewAdapter.PlayerListEntryViewHolder>() {

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlayerListEntryViewHolder {
    val listEntry = LayoutInflater.from(parent.context)
        .inflate(R.layout.new_game_recyclerview_entry, parent, false) as LinearLayout
    return PlayerListEntryViewHolder(listEntry)
  }

  override fun onBindViewHolder(holder: PlayerListEntryViewHolder, position: Int) {
    val currentUser = players.elementAt(position)
    val addFriendButton = holder.itemView.add_friend_button
    val removePlayerButton = holder.itemView.remove_player_button

    holder.itemView.new_player_name_textview.text =
        String.format("%s %s", currentUser.firstName, currentUser.lastName)

    //TODO HIDE IF ALREADY FRIENDS
//    addFriendButton.visibility = View.VISIBLE

    if (gameOwner != currentUser) {
      removePlayerButton.setOnClickListener {
        players.remove(players.elementAt(holder.adapterPosition))
        notifyDataSetChanged()
      }
    } else {
      // you don't want to add yourself as a friend or remove yourself from the game
      // you should be nicer to yourself
      addFriendButton.visibility = View.GONE
      removePlayerButton.visibility = View.GONE
    }

  }

  override fun getItemCount(): Int {
    return players.size
  }

  class PlayerListEntryViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}
