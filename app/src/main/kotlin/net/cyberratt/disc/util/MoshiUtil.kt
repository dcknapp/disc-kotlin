package net.cyberratt.disc.util

import com.squareup.moshi.Moshi

inline fun <reified T: Any?> Moshi.fromJson(source: String): T {
  return this.adapter(T::class.java).fromJson(source)!!
}

inline fun <reified T: Any?> Moshi.toJson(source: T): String {
  return this.adapter(T::class.java).toJson(source)
}