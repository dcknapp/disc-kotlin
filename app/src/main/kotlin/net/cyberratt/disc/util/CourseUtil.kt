package net.cyberratt.disc.util

import net.cyberratt.disc.db.model.Course

object CourseUtil {

  val defaultCourse: Course = Course(name = "Pleasuretown")
}