package net.cyberratt.disc.util

fun <T : Any> T.TAG() = this::class.simpleName