package net.cyberratt.disc.util

import android.content.Context
import net.cyberratt.disc.R
import net.cyberratt.disc.db.model.User
import java.util.*

object UserUtil {

  val TAG: String = UserUtil::class.java.simpleName

//  fun isFriend(context: Context, currentUser: User, gameOwner: User): Boolean {
//    // TODO ADD LOCAL FRIEND LIST AND CHECK AGAINST IT
//    return true
//  }

  /**
   * Constructs a user object from a provided name string

   * @param name name of user to be created
   * *
   * @return user object
   */
  fun createUserWithName(name: String): User {
    if (!TextUtil.isPopulated(name)) {
      throw IllegalArgumentException("Name cannot be blank")
    }

    val (firstName, lastName) = StringUtil.splitNameString(name)
    return User(firstName = firstName, lastName = lastName)
  }

  /**
   * Constructs a default user object

   * @param context context for retrieving strings
   * *
   * @return user created from default dummy data
   */
  fun getDefaultUser(context: Context): User {
    val (firstName, lastName) = StringUtil.splitNameString(context.getString(R.string.player_default_name))
    return User(
        id = UUID.fromString(context.getString(R.string.zero_uuid)),
        firstName = firstName,
        lastName = lastName)
  }

}
