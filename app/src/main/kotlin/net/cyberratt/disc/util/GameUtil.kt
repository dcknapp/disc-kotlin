package net.cyberratt.disc.util

import net.cyberratt.disc.db.model.Game
import net.cyberratt.disc.db.model.User

object GameUtil {

  /**
   * Creates a game for multiple users
   * @param users list of users in game
   * *
   * @return game containing all users
   */
  fun createGameForUserList(users: List<User>): Game {
    if (users.isEmpty()) {
      throw IllegalStateException("Cannot create game with no users")
    }

    return Game().apply {
      users.map { user -> addScore(ScoreUtil.buildDefaultScoreForUser(user)) }
    }
  }
}
