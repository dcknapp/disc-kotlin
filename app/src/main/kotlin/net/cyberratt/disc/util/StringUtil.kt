package net.cyberratt.disc.util

object StringUtil {

  fun splitNameString(name: String): Pair<String, String> {
    val tokens = name.split(limit = 2, delimiters = ' ')

    return Pair(tokens[0], if (tokens.size > 1) tokens[1] else "")
  }
}