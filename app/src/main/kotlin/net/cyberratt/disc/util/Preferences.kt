package net.cyberratt.disc.util

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import com.squareup.moshi.Moshi
import net.cyberratt.disc.db.model.Course
import net.cyberratt.disc.db.model.User

open class Preferences constructor(
    private val sharedPreferences: SharedPreferences,
    private val moshi: Moshi,
    private var context: Context) {

  /**
   * Retrieves logged in user from shared preferences

   * @return user currently logged in user
   */
  open var loggedInUser: User?
    get() {
      var user: User? = null
      sharedPreferences.getString(Keys.LOGGED_IN_USER, null)?.let {
        user = moshi.fromJson(it)
      }
      return user
    }
  @SuppressLint("ApplySharedPref")
    set(user) {
      sharedPreferences.edit()
          .putString(Keys.LOGGED_IN_USER, moshi.toJson(user))
          .commit()
    }

  /**
   * Retrieves stored list of users in shared preferences.

   * @return stored list of users
   */
  open var currentGameUsers: List<User>?
    get() {
      var users: List<User> = emptyList()
      sharedPreferences?.getString(Keys.CURRENT_GAME_USERS, null)?.let {
        users = moshi.fromJson(it)
      }
      return users
    }
    set(players) = sharedPreferences?.edit()
        .putString(Keys.CURRENT_GAME_USERS, moshi.toJson(players))
        .apply()

  /**
   * Retrieves the currently logged in user object

   * @return logged in user or default dummy user
   */
  val loggedInOrDefaultUser: User
    get() = loggedInUser ?: UserUtil.getDefaultUser(context)

  /**
   * Logs out the current user
   */
  @SuppressLint("ApplySharedPref")
  fun logOutCurrentUser() {
    sharedPreferences.edit()
        .remove(Keys.LOGGED_IN_USER)
        .commit()
  }

  var currentCourse: Course
    get() {
      sharedPreferences.getString(Keys.CURRENT_COURSE, null)?.let {
        return moshi.fromJson(it)
      }
      return CourseUtil.defaultCourse
    }
    set(value) {
      val courseString = moshi.toJson(value)
      sharedPreferences
          .edit()
          .putString(Keys.CURRENT_COURSE, courseString)
          .apply()
    }

  object Keys {

    private val KEY_PREFIX = "net.cyberratt.disc."

    val CURRENT_GAME_USERS = "${KEY_PREFIX}CURRENT_GAME_USERS"
    val CURRENT_COURSE = "${KEY_PREFIX}CURRENT_COURSE"
    val DEFAULT_SHARED_PREFS = "${KEY_PREFIX}DEFAULT_SHARED_PREFS"
    val LOGGED_IN_USER = "${KEY_PREFIX}LOGGED_IN_USER"

  }
  companion object {
    val TAG: String = Preferences::class.java.simpleName

  }
}
