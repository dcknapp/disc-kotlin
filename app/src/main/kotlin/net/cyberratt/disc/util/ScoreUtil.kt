package net.cyberratt.disc.util

import android.util.SparseIntArray
import net.cyberratt.disc.db.data.HoleReview
import net.cyberratt.disc.db.model.Score
import net.cyberratt.disc.db.model.User

object ScoreUtil {

  val TAG: String = ScoreUtil::class.java.simpleName

  /**
   * Gets lowest score from a game to determine lead differentials

   * @param scores map of ids and scores representing a game
   * *
   * @return integer containing lowest score
   */
  fun getLowestScore(scores: List<Score>): Int? {
    val (lowestScore, _) = getLowestTwoScores(scores)

    return lowestScore.totalScore
  }

  /**
   * Retrieves the second-place score from a game

   * @param scores map of ids and scores representing a game
   * *
   * @return integer containing lowest score
   */
  fun getSecondLowestScore(scores: List<Score>): Int? {
    if (scores.size == 1) {
      return scores.elementAt(0).totalScore
    }

    val (_, secondLowestScore) = getLowestTwoScores(scores)

    return secondLowestScore.totalScore
  }

  /**
   * Determines whether a game represented by a map of IDs and Scores is a tie for 1st place

   * @param scores map of scores representing a game
   * *
   * @return boolean whether a score is a tie or not
   */
  fun isScoreTied(scores: List<Score>): Boolean {
    if (scores.size < 2)
      return false

    val (lowestScore, secondLowestScore) = getLowestTwoScores(scores)

    return lowestScore.totalScore == secondLowestScore.totalScore
  }

  /**
   * Sorts score list by total score and returns the lowest 2 scores
   *
   * @param scores collection of scores
   *
   * @return 2-element list of lowest scores
   */
  private fun getLowestTwoScores(scores: List<Score>): List<Score> {
    return if (scores.size > 1) {
      scores.sortedBy { it.totalScore }.take(2)
    } else {
      listOf(scores[0], scores[0])
    }
  }

  /**
   * Converts a hole-cardHoleNumber-keyed map to a delimited score string.

   * @param scores hole-numbered map of scores
   * *
   * @return '|'-delimited string of hole scores
   */
  fun toDelimitedString(scores: SparseIntArray): String {
    var scoreString = ""
    for (i in 1..scores.size()) {
      scoreString += scores[i].toString()
      if (i < scores.size()) {
        scoreString += "|"
      }
    }

    return scoreString
  }

  fun getScoreForHole(delimitedString: String, hole: Int): Int {
    return toHoleNumberedMap(delimitedString)[hole]
  }

  /**
   * Converts a delimited score string to a map of scores.
   * The key is the hole cardHoleNumber, the value is the score.

   * @param delimitedString '|'-separated string of hole scores
   * *
   * @return map of hole numbers to scores
   */
  fun toHoleNumberedMap(delimitedString: String): SparseIntArray {
    val scoreTokens = delimitedString.split('|')

    val scoreMap = SparseIntArray()
    for (i in 1..scoreTokens.size) {
      scoreMap.put(i, Integer.parseInt(scoreTokens[i - 1]))
    }

    return scoreMap
  }

  /**
   * Converts a delimited score string to a list of integer score values

   * @param delimitedString '|'-separated string of hole scores
   * *
   * @return List of hole scores
   */
  fun toScoreList(delimitedString: String): List<Int> {
    val scoreMap = toHoleNumberedMap(delimitedString)

    return (1..scoreMap.size()).map { scoreMap[it] }
  }

  /**
   * Retrieves the score for the second half holes from a delimited score string.

   * @param delimitedString  '|'-separated string of hole scores
   * *
   * @return total score from the back half of a game's holes
   */
  fun getBackHalfScore(delimitedString: String): Int {
    return getHalfScoreFromScoreList(toScoreList(delimitedString), GameHalf.BACK)
  }

  /**
   * Retrieves the score for the first half holes from a delimited score string.

   * @param delimitedString  '|'-separated string of hole scores
   * *
   * @return total score from the front half of a game's holes
   */
  fun getFrontHalfScore(delimitedString: String): Int {
    return getHalfScoreFromScoreList(toScoreList(delimitedString), GameHalf.FRONT)
  }

  /**
   * Collects scores given a specified desired half of a game.
   * If course has even holes, should split the holes evenly.
   * If odd holes, front score should contain the extra hole's score.

   * @param scores list of hole scores
   * *
   * @param half enum value representing front or back set of holes for a course
   * *
   * @return total score value from a specified half (back/front) of a game
   */
  private fun getHalfScoreFromScoreList(scores: List<Int>, half: GameHalf): Int {
    if (scores.size <= 9)
      return scores.sum()

    val start: Int
    val end: Int

    if (half == GameHalf.FRONT) {
      start = 0
      end = getFirstBackHalfHoleNumber(scores.size) - 1
    } else {
      start = getFirstBackHalfHoleNumber(scores.size) - 1
      end = scores.size
    }

    return scores.subList(start, end).sum()
  }

  /**
   * Calculates the halfway point of a set of holes to divide the front and back scores.

   * @param numHoles cardHoleNumber of holes
   * *
   * @return first hole cardHoleNumber of the "back half"
   */
  fun getFirstBackHalfHoleNumber(numHoles: Int): Int {
    return when (numHoles % 2) {
      0 -> numHoles / 2 + 1
      else -> numHoles / 2 + 2
    }
  }

  /**
   * Calculates the total of a given delimited score string.

   * @param delimitedString '|'-separated string of hole scores
   * *
   * @return int containing total score
   */
  fun getTotalScore(delimitedString: String): Int {
    return toScoreList(delimitedString).sum()
  }

  /**
   * @return score string for a default 18 hole course with zero values
   */
  fun buildDefaultScoreString() =  buildDefinedLengthZeroScoreString(18)

  /**
   * Returns a '|'-delimited string of 0s of a specified length

   * @param length cardHoleNumber of holes for which to create a score string
   * *
   * @return '|'-delimited string of 'length' zeroed hole scores
   */
  private fun buildDefinedLengthZeroScoreString(length: Int): String {
    var scoreString = ""
    for (i in 0 until length) {
      scoreString += "0"
      if (i < length - 1) {
        scoreString += "|"
      }
    }
    return scoreString
  }

  /**
   * Creates a score object containing specified User

   * @param user user whose score should be created
   * *
   * @return score for appropriate user
   */
  fun buildDefaultScoreForUser(user: User): Score {
    return Score(user = user, scoreString = ScoreUtil.buildDefaultScoreString())
  }

  /**
   * Retrieves score object containing a specific user id from a list of scores

   * @param scores current game scores
   * @param player player whose score is being retrieved.
   * *
   * @return score of desired player
   */
  fun getScoreForUser(scores: Collection<Score>, player: User) = scores.first { it.user?.id == player.id }

  /**
   * Calculates the score difference for a score based on its card id
   */
  fun calculateScoreDifference(scores: List<Score>, player: User): Int {
    val playerScore = getScoreForUser(scores, player)
    val (lowestScore, secondLowestScore) = getLowestTwoScores(scores)
    val diff = playerScore.totalScore - lowestScore.totalScore

    return when(isPlayerWinning(diff, scores)) {
      true -> lowestScore.totalScore - secondLowestScore.totalScore
      false -> diff
    }
  }

  private fun isPlayerWinning(diff: Int, scores: List<Score>) = diff == 0 && !isScoreTied(scores)

  private enum class GameHalf {
    FRONT, BACK
  }

  fun holeReviewForScores(scores: List<Score>, holeNumber: Int): List<HoleReview> {
    return scores.map {
      HoleReview(
          playerName = it.user?.firstName!!,
          holeScore = it.holeNumberedScoreMap[holeNumber],
          totalScore = calculateScoreDifference(scores, it.user!!)
      )
    }
  }
}
