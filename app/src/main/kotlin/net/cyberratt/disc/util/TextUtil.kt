package net.cyberratt.disc.util

import android.widget.EditText

object TextUtil {

  /**
   * Convenience method to check if EditText views are populated

   * @param editTexts varargs array of EditText objects
   * *
   * @return true if all contain text, false if
   */
  fun isPopulated(vararg editTexts: EditText?): Boolean {
    return editTexts.none { it == null || it.text == null || it.text.toString().isEmpty() }
  }

  /**
   * Convenience method to reduce boilerplate string checks
   * @param strings String to determine populated state
   * *
   * @return true or false
   */
  fun isPopulated(vararg strings: String?): Boolean {
    return strings.none { it == null || it.isEmpty() }
  }
}
