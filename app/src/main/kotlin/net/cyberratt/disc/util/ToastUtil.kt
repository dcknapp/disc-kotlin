package net.cyberratt.disc.util

import android.content.Context
import android.widget.Toast
import net.cyberratt.disc.R

object ToastUtil {

  /**
   * Displays default toast for WIP features
   * @param context for toast display reference
   */
  fun displayNotYetImplementedToast(context: Context) {
    displayToast(context, context.resources.getString(R.string.not_yet_implemented))
  }

  /**
   * Wrapper method for displaying a toast with a specific message
   * @param context for toast display reference
   * *
   * @param message message to display
   */
  fun displayToast(context: Context, message: String) {
    Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
  }
}
