package net.cyberratt.disc.ui.activity

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.intent.Intents.intended
import android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent
import android.support.test.espresso.intent.rule.IntentsTestRule
import android.support.test.espresso.matcher.ViewMatchers.withId
import com.nhaarman.mockito_kotlin.whenever
import net.cyberratt.disc.R
import net.cyberratt.disc.dagger.ComponentHolder
import net.cyberratt.disc.dagger.DaggerTest
import net.cyberratt.disc.db.model.User
import net.cyberratt.disc.util.Preferences
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import timber.log.Timber
import javax.inject.Inject

class MainActivityTest : DaggerTest() {

  @Inject lateinit var preferences: Preferences

  @get:Rule
  private val mainActivityTestRule = IntentsTestRule(MainActivity::class.java)

  @Before override fun setUp() {
    super.setUp()

    testComponent.inject(this)

    whenever(preferences.loggedInUser).thenReturn(user())
    whenever(preferences.currentGameUsers).thenReturn(emptyList())

    mainActivityTestRule.launchActivity(null)
  }

  private fun user() = User(firstName = "test", lastName = "user")

  @Test fun clickMainFabButton_launchGameSetupActivity() {
    onView(withId(R.id.main_fab)).perform(click())
    intended(hasComponent(GameSetupActivity::class.java.name))
  }

}
