package net.cyberratt.disc.rx

import net.cyberratt.disc.dagger.DaggerTest
import org.junit.Before
import org.junit.Test
import javax.inject.Inject

class ScoreChangeListenerTest : DaggerTest() {

  @Inject lateinit var mScoreChangeListener: ScoreChangeListener

  @Before fun setup() {
    testComponent.inject(this)
  }

  @Test fun relayRelays() {
    val testObserver = mScoreChangeListener.relay().test()
    testObserver.assertEmpty()
    mScoreChangeListener.update()
    testObserver.assertNoErrors()

    testObserver.assertValueCount(1)
  }

}
