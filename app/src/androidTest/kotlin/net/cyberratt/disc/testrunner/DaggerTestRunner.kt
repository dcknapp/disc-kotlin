package net.cyberratt.disc.testrunner

import android.support.test.runner.AndroidJUnitRunner

class DaggerTestRunner: AndroidJUnitRunner() {

  @Throws(InstantiationException::class, IllegalAccessException::class, ClassNotFoundException::class)
  override fun newApplication(classLoader: ClassLoader, className: String, context: android.content.Context): android.app.Application {
    return super.newApplication(classLoader, net.cyberratt.disc.TestDiscApplication::class.java.name, context)
  }

}
