package net.cyberratt.disc.dagger

import dagger.Component
import net.cyberratt.disc.dagger.modules.ContextModule
import net.cyberratt.disc.dagger.modules.MockPreferencesModule
import net.cyberratt.disc.dagger.modules.MockRestModule
import net.cyberratt.disc.dagger.modules.MoshiModule
import net.cyberratt.disc.rx.ScoreChangeListenerTest
import net.cyberratt.disc.ui.activity.MainActivityTest
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(
    ContextModule::class,
    MockRestModule::class,
    MockPreferencesModule::class,
    MoshiModule::class)
)
interface TestCoreComponent: CoreComponent {
  fun inject(mainActivityTest: MainActivityTest)

  fun inject(scoreListenerTest: ScoreChangeListenerTest)
}
