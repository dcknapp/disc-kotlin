package net.cyberratt.disc.dagger.modules

import com.nhaarman.mockito_kotlin.mock
import dagger.Module
import dagger.Provides
import net.cyberratt.disc.network.DriverRestService
import javax.inject.Singleton

@Module
open class MockRestModule {

  @Provides
  @Singleton
  fun provideDriverRestClient(): DriverRestService = mock()

}