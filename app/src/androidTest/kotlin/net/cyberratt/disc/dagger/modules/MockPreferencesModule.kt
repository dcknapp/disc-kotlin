package net.cyberratt.disc.dagger.modules

import android.content.SharedPreferences
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import dagger.Module
import dagger.Provides
import net.cyberratt.disc.util.Preferences
import javax.inject.Singleton

@Module
class MockPreferencesModule {

  @Provides
  @Singleton
  fun provideMockSharedPreferences(): SharedPreferences {
    val mockPrefs: SharedPreferences = mock()
    val mockEditor: SharedPreferences.Editor = mock()

    whenever(mockPrefs.edit()).thenReturn(mockEditor)
    whenever(mockEditor.putString(any(), any())).thenReturn(mockEditor)

    return mockPrefs
  }

  @Provides
  @Singleton
  fun provideMockPreferences(): Preferences = mock()

}