package net.cyberratt.disc.dagger

import android.support.annotation.CallSuper
import android.support.test.InstrumentationRegistry
import com.squareup.moshi.Moshi
import net.cyberratt.disc.dagger.modules.ContextModule
import net.cyberratt.disc.network.typeadapter.JodaDateTimeMoshiAdapter
import net.cyberratt.disc.network.typeadapter.UuidMoshiAdapter
import org.junit.Before
import timber.log.Timber

open class DaggerTest {

  lateinit var testComponent: TestCoreComponent

  val moshi by lazy {
    Moshi.Builder()
        .add(JodaDateTimeMoshiAdapter())
        .add(UuidMoshiAdapter())
        .build()
  }

  @Before
  @CallSuper
  open fun setUp() {
    ComponentHolder.setCore(buildTestComponent())
    testComponent = ComponentHolder.getCore() as TestCoreComponent
  }

  private fun buildTestComponent() =
      DaggerTestCoreComponent.builder()
          .contextModule(ContextModule(getContext()))
          .build()

  private fun getContext() = InstrumentationRegistry.getInstrumentation().targetContext
}
