package net.cyberratt.disc

import net.cyberratt.disc.dagger.DaggerTestCoreComponent
import net.cyberratt.disc.dagger.TestCoreComponent
import net.cyberratt.disc.dagger.modules.ContextModule

class TestDiscApplication: DiscApplication() {
  override fun buildComponent(): TestCoreComponent {
    return DaggerTestCoreComponent.builder()
        .contextModule(ContextModule(this))
        .build()
  }
}
